/**
 * Tail Manager
 */
class Tail {
    
    /**
     * Tail Manager constructor
     */
    constructor() {
        this.amount      = 15;
        this.minDistance = 8;
        this.elements    = [];
        this.container   = document.querySelector(".tail");
        this.container.innerHTML = "";
        
        for (let i = 0; i < this.amount; i += 1) {
            const div = document.createElement("DIV");
            this.container.appendChild(div);
            
            this.elements.push({
                element : div,
                top     : 0,
                left    : 0,
            });
        }
    }
    
    /**
     * Sets the initial positions of the tails elements
     * @param {Ball} ball
     * @returns {Void}
     */
    start(ball) {
        const pos = ball.getPosition();
        for (const data of this.elements) {
            data.top  = pos.top;
            data.left = pos.left;
        }
        this.setPosition();
    }
    
    /**
     * Move the tail
     * @param {Ball} ball
     * @returns {Void}
     */
    move(ball) {
        const first = this.elements[0];
        const pos   = ball.getPosition();
        let   top   = pos.top;
        let   left  = pos.left;
        
        if (Math.abs(top - first.top) < this.minDistance ||
                Math.abs(left - first.left) < this.minDistance) {
            return;
        }

        for (const data of this.elements) {
            const oldTop  = data.top;
            const oldLeft = data.left;
            
            data.top  = top;
            data.left = left;
            
            top  = oldTop;
            left = oldLeft;
        }
        this.setPosition();
    }
    
    /**
     * Sets the position of each element
     * @returns {Void}
     */
    setPosition() {
        for (const data of this.elements) {
            data.element.style.top  = data.top  + "px";
            data.element.style.left = data.left + "px";
        }
    }
}
