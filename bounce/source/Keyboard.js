/**
 * The Game Keyboard
 */
class Keyboard {
    
    /**
     * The Game Keyboard constructor
     * @param {Display} display
     * @param {Scores}  scores
     * @param {Object}  shortcuts
     */
    constructor(display, scores, shortcuts) {
        this.fastKeys   = [ 37, 65, 39, 68 ];
        this.shortcuts  = shortcuts;
        this.keyPressed = null;
        
        this.display    = display;
        this.scores     = scores;
        
        document.addEventListener("keydown", (e) => this.onKeyDown(e));
        document.addEventListener("keyup",   (e) => this.onKeyUp(e));
    }
    
    
    /**
     * Key handler for the on key down event
     * @param {Event} event
     * @returns {Void}
     */
    onKeyDown(event) {
        if (this.display.isPlaying() && this.fastKeys.indexOf(event.keyCode) > -1) {
            if (this.keyPressed === null) {
                this.keyPressed = event.keyCode;
            } else {
                return;
            }
        }
        this.pressKey(event.keyCode, event);
    }
    
    /**
     * Key handler for the on key up event
     * @param {Event} event
     * @returns {Void}
     */
    onKeyUp() {
        this.keyPressed = null;
    }
    
    /**
     * When a key is pressed, this is called on each frame for fast key movements
     * @returns {Void}
     */
    onKeyHold() {
        if (this.keyPressed !== null && this.display.isPlaying()) {
            this.pressKey(this.keyPressed);
        }
    }
    
    
    /**
     * Key Press Event
     * @param {Number} key
     * @param {?Event} event
     * @returns {Void}
     */
    pressKey(key, event) {
        if (this.scores.isFocused()) {
            if (key === 13) {
                // eslint-disable-next-line new-cap
                this.shortcuts.gameOver.O();
            }
        } else {
            if (!this.display.isPlaying()) {
                event.preventDefault();
            }
            let keyCode = key;
            if ([ 69, 49, 97 ].indexOf(key) > -1) {           // E / 1
                keyCode = "E";
            } else if ([ 82, 50, 98 ].indexOf(key) > -1) {    // R / 2
                keyCode = "R";
            } else if ([ 75, 51, 99 ].indexOf(key) > -1) {    // K / 3
                keyCode = "C";
            } else if ([ 8, 66, 78 ].indexOf(key) > -1) {     // Backspace / B / N
                keyCode = "B";
            } else if ([ 13, 32, 79 ].indexOf(key) > -1) {    // Enter / Space / O
                keyCode = "O";
            } else if ([ 80, 67 ].indexOf(key) > -1) {        // P / C
                keyCode = "P";
            } else if ([ 37, 65 ].indexOf(key) > -1) {        // Left  / A
                keyCode = "A";
            } else if ([ 39, 68 ].indexOf(key) > -1) {        // Right / D
                keyCode = "D";
            } else {
                keyCode = String.fromCharCode(key);
            }
            
            if (this.shortcuts[this.display.get()][keyCode]) {
                this.shortcuts[this.display.get()][keyCode]();
            }
        }
    }
}
