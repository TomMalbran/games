/**
 * The Game Class
 */
class Game {
    /**
     * The main Function
     */
    constructor() {
        this.initDomListeners();
        this.initShortcuts();
        
        this.soundFiles = [ "start", "eat", "end" ];

        this.display  = new Display();
        this.score    = new Score(this.display);
        this.board    = new Board();
        this.demo     = new Demo(this.board);
        this.instance = new Instance(this.board);
        this.sound    = new Sounds(this.soundFiles, "snake.sound", true);
        this.scores   = new HighScores();
        this.keyboard = new Keyboard(this.display, this.scores, this.shortcuts);
    }

    /**
     * Stores the used DOM elements and initializes the Event Handlers
     * @returns {Void}
     */
    initDomListeners() {
        this.navigator = document.querySelector(".main ul");
        this.starter   = document.querySelector(".start");
        
        document.body.addEventListener("click", (e) => {
            const element = Utils.getTarget(e);
            const actions = {
                play       : () => this.newGame(element.dataset.level),
                mainScreen : () => this.showMainScreen(),
                highScores : () => this.showHighScores(),
                help       : () => this.showHelp(),
                restore    : () => this.restoreGame(),
                endPause   : () => this.endPause(),
                finishGame : () => this.finishGame(),
                save       : () => this.endGameOver(true),
                newGame    : () => this.endGameOver(false),
                showScores : () => this.scores.show(element.dataset.level),
                sound      : () => this.sound.toggle(),
            };
            
            if (actions[element.dataset.action]) {
                actions[element.dataset.action]();
            }
        });
        
        this.navigator.addEventListener("mouseover", (e) => {
            const element = e.target.dataset.action ? e.target : e.target.parentElement;
            if (element.dataset.action === "play") {
                this.startDemo(element.dataset.level);
            }
        });
        this.navigator.addEventListener("mouseout", (e) => {
            const element = e.target.dataset.action ? e.target : e.target.parentElement;
            if (element.dataset.action === "play") {
                this.endDemo();
            }
        });
        
        document.querySelector(".snake").addEventListener("click", (e) => {
            if (this.display.isPlaying()) {
                if (this.snake.mouseTurn(e)) {
                    this.instance.saveDirection(this.snake.getDirection());
                }
            }
        });
    }

    /**
     * Returns the shortcut functions
     * @returns {Object}
     */
    initShortcuts() {
        this.shortcuts = {
            mainScreen : {
                O : () => this.newGame(this.score.level),
                Y : () => this.newGame(1),
                E : () => this.newGame(2),
                R : () => this.newGame(3),
                U : () => this.newGame(4),
                I : () => this.showHighScores(),
                H : () => this.showHelp(),
                T : () => this.restoreGame(),
                M : () => this.sound.toggle(),
            },
            paused : {
                P : () => this.endPause(),
                B : () => this.finishGame(),
            },
            gameOver : {
                O : () => this.endGameOver(true),
                B : () => this.endGameOver(false),
            },
            highScores : {
                Y : () => this.scores.show(1),
                E : () => this.scores.show(2),
                R : () => this.scores.show(3),
                U : () => this.scores.show(4),
                B : () => this.showMainScreen(),
            },
            help : {
                B : () => this.showMainScreen(),
            },
            playing : {
                W : () => this.turnSnake(-1, 0),
                A : () => this.turnSnake(0, -1),
                S : () => this.turnSnake(1, 0),
                D : () => this.turnSnake(0, 1),
                P : () => this.startPause(),
                M : () => this.sound.toggle(),
            },
            saveHighScore : () => this.saveHighScore(),
        };
    }

    /**
     * Turns the Snake
     * @param {Number} dirTop
     * @param {Number} dirLeft
     * @returns {Void}
     */
    turnSnake(dirTop, dirLeft) {
        if (this.snake.turn(dirTop, dirLeft)) {
            this.instance.saveDirection(this.snake.getDirection());
        }
    }
    
    
    
    /**
     * Starts a new game
     * @param {Number} level
     * @returns {Void}
     */
    newGame(level) {
        this.display.set("starting").setClass();
        this.score.set(level).show();
        
        this.matrix = new Matrix(this.board, this.instance);
        this.snake  = new Snake(this.board, this.matrix);
        this.food   = new Food(this.board, this.matrix.addFood());
        
        this.instance.newGame(level);
        this.requestAnimation();
    }

    /**
     * Destroys the game elements
     * @returns {Void}
     */
    destroyGame() {
        this.matrix = null;
        this.snake  = null;
        this.food   = null;
        this.instance.destroyGame();
    }
    
    /**
     * Reduces by 1 the initial count until it changes the mode to playing
     * @returns {Void}
     */
    nextCount() {
        let content = "";
        
        this.score.decCount();
        if (this.score.count > 0) {
            content = this.score.count;
            this.sound.start();
        } else if (this.score.count === 0) {
            content = "Go!";
            this.sound.eat();
            window.setTimeout(() => this.sound.eat(), 200);
        } else {
            this.display.set("playing").setClass();
        }
        this.starter.innerHTML = content;
    }
    
    /**
     * Request an animation frame
     * @returns {Void}
     */
    requestAnimation() {
        this.startTime = new Date().getTime();
        this.animation = window.requestAnimationFrame(() => {
            const time  = new Date().getTime() - this.startTime;
            const speed = time / 16;
            
            this.score.decTime(time);
            if (speed <= 0 || speed > 5) {
                this.requestAnimation();
                return;
            }
            
            if (this.score.time < 0) {
                if (this.display.isDemoing()) {
                    this.demo.move();
                }
                if (this.display.isStarting()) {
                    this.nextCount();
                } else if (this.display.isPlaying()) {
                    const res = this.snake.move();
                    if (res === "crashed") {
                        this.sound.end();
                        this.gameOver();
                    } else if (res === "ate") {
                        this.sound.eat();
                        this.score.incScore(this.food.getTimer());
                        this.instance.saveScore(this.score.score);
                        this.food.add(this.matrix.addFood());
                    }
                }
                this.score.resetTime();
            }
            if (this.display.isPlaying()) {
                this.food.reduceTime(time);
                this.score.showFoodTimer(this.food.getTimer());
            }
            
            if (this.display.isDemoing() || this.display.isStarting() || this.display.isPlaying()) {
                this.requestAnimation();
            }
        });
    }
    
    /**
     * Cancel an animation frame
     * @returns {Void}
     */
    cancelAnimation() {
        window.cancelAnimationFrame(this.animation);
    }
    
    
    
    /**
     * Show the Main Screen
     * @returns {Void}
     */
    showMainScreen() {
        this.display.set("mainScreen").show();
    }
    
    /**
     * Pause the Game
     * @returns {Void}
     */
    startPause() {
        this.display.set("paused").show();
        this.cancelAnimation();
    }
    
    /**
     * Unpause the Game
     * @returns {Void}
     */
    endPause() {
        this.display.set("playing").setClass();
        this.requestAnimation();
    }
    
    /**
     * Finish the Game
     * @returns {Void}
     */
    finishGame() {
        this.display.set("mainScreen").show();
        this.destroyGame();
        this.cancelAnimation();
        this.instance.destroyGame();
    }
    
    /**
     * Game Over
     * @returns {Void}
     */
    gameOver() {
        this.cancelAnimation();
        this.display.set("gameOver").show();
        this.scores.setInput();
        this.instance.destroyGame();
    }
    
    /**
     * Svae scores and restart
     * @param {Boolean} save
     * @returns {Void}
     */
    endGameOver(save) {
        this.destroyGame();
        if (save) {
            this.saveHighScore();
        } else {
            this.showMainScreen();
        }
    }
    
    
    /**
     * Starts the speed demo
     * @param {Number} level
     * @returns {Void}
     */
    startDemo(level) {
        this.display.set("demo");
        this.score.set(level, 0);
        this.demo.start(level);
        this.requestAnimation();
    }
    
    /**
     * Ends the speed demo
     * @returns {Void}
     */
    endDemo() {
        if (this.display.isDemoing()) {
            this.display.set("mainScreen");
            this.cancelAnimation();
        }
        this.demo.end();
    }
    
    /**
     * Show the High Scores
     * @returns {Void}
     */
    showHighScores() {
        this.display.set("highScores").show();
    }
    
    /**
     * Saves a High Score
     * @returns {Void}
     */
    saveHighScore() {
        if (this.scores.save(this.score.level, this.score.score)) {
            this.showHighScores();
        }
    }
    
    /**
     * Show the Help
     * @returns {Void}
     */
    showHelp() {
        this.display.set("help").show();
    }
    
    
    
    /**
     * Restores a saved Game
     * @returns {Void}
     */
    restoreGame() {
        if (this.instance.hasGame()) {
            const data = this.instance.getData();
            
            this.display.set("continuing").show();
            this.score.set(data.level, data.score).show();
            
            this.matrix = new Matrix(this.board, this.instance, data.matrix, data.head, data.tail);
            this.snake  = new Snake(this.board, this.matrix, data.links, data.dirTop, data.dirLeft);
            this.food   = new Food(this.board, null, data.foodTop, data.foodLeft);
        }
    }
}



// Load the game
window.addEventListener("load", function () {
    "use strict";
    const game = new Game();
}, false);
