/**
 * The Game Class
 */
class Game {
    /**
     * The main Function
     */
    constructor() {
        this.initDomListeners();
        this.initShortcuts();
        
        this.soundFiles      = [ "pause", "crash", "drop", "line", "rotate", "end" ];
        this.maxInitialLevel = 10;
        this.tetriminoSize   = 2;

        this.display  = new Display();
        this.level    = new Level(this.maxInitialLevel);
        this.sound    = new Sounds(this.soundFiles, "tetris.sound", true);
        this.scores   = new HighScores();
        this.keyboard = new Keyboard(this.display, this.scores, this.shortcuts);

        // let board, score, tetriminos;
    }

    /**
     * Stores the used DOM elements and initializes the Event Handlers
     * @returns {Void}
     */
    initDomListeners() {
        document.body.addEventListener("click", (e) => {
            const element = Utils.getTarget(e);
            const actions = {
                decrease   : () => this.level.dec(),
                increase   : () => this.level.inc(),
                start      : () => this.newGame(),
                mainScreen : () => this.showMainScreen(),
                endPause   : () => this.endPause(),
                pause      : () => this.showPause(),
                finishGame : () => this.finishGame(),
                highScores : () => this.showHighScores(),
                help       : () => this.showHelp(),
                save       : () => this.saveHighScore(),
                restore    : () => this.scores.restore(),
                sound      : () => this.sound.toggle(),
            };
            
            if (actions[element.dataset.action]) {
                actions[element.dataset.action]();
            }
        });
    }

    /**
     * Creates the shortcuts functions
     * @returns {Object}
     */
    initShortcuts() {
        this.shortcuts = {
            mainScreen : {
                O : () => this.newGame(),
                A : () => this.level.dec(),
                I : () => this.showHighScores(),
                D : () => this.level.inc(),
                H : () => this.showHelp(),
                M : () => this.sound.toggle(),
            },
            paused : {
                P : () => this.endPause(),
                B : () => this.finishGame(),
            },
            gameOver : {
                O : () => this.saveHighScore(),
                B : () => this.showMainScreen(),
            },
            highScores : {
                B : () => this.showMainScreen(),
                R : () => this.scores.restore(),
            },
            help : {
                B : () => this.showMainScreen(),
            },
            playing : {
                C : () => this.tetriminos.hardDrop(),
                W : () => this.tetriminos.rotateRight(),
                A : () => this.tetriminos.moveLeft(),
                S : () => this.tetriminos.softDrop(),
                D : () => this.tetriminos.moveRight(),
                X : () => this.tetriminos.rotateRight(),
                Z : () => this.tetriminos.rotateLeft(),
                P : () => this.startPause(),
                M : () => this.sound.toggle(),
            },
            number : (number) => {
                if (this.display.inMainScreen()) {
                    this.level.choose(number);
                }
            },
        };
    }

    
    
    /**
     * Starts a new game
     * @returns {Void}
     */
    newGame() {
        this.display.set("playing").hide();
        this.keyboard.reset();
        
        this.board      = new Board(this.tetriminoSize, this.onWindEnd.bind(this));
        this.score      = new Score(this.level.get(), this.maxInitialLevel);
        this.tetriminos = new Tetriminos(this.board, this.sound, this.score, this.tetriminoSize, this.showGameOver.bind(this));
        
        this.requestAnimation();
    }

    /**
     * Called when a wink ends
     * @returns {Void}
     */
    onWindEnd() {
        this.tetriminos.setHardDrop();
        this.requestAnimation();
    }

    /**
     * Request an animation frame
     * @returns {Void}
     */
    requestAnimation() {
        this.startTime = new Date().getTime();
        this.animation = window.requestAnimationFrame(() => {
            const time = new Date().getTime() - this.startTime;
            
            this.score.decTime(time);
            if (this.score.time < 0) {
                this.tetriminos.softDrop();
                this.score.resetTime();
            }
            this.keyboard.holdingKey();
            
            if (this.display.isPlaying() && !this.board.isWinking()) {
                this.requestAnimation();
            }
        });
    }
    
    /**
     * Cancel an animation frame
     * @returns {Void}
     */
    cancelAnimation() {
        window.cancelAnimationFrame(this.animation);
    }
    
    
    
    /**
     * Show the Main Screen
     * @returns {Void}
     */
    showMainScreen() {
        this.display.set("mainScreen").show();
    }
    
    /**
     * Pause the Game
     * @returns {Void}
     */
    startPause() {
        this.display.set("paused").show();
        this.sound.pause();
        this.cancelAnimation();
    }
    
    /**
     * Unpause the Game
     * @returns {Void}
     */
    endPause() {
        this.display.set("playing").hide();
        this.sound.pause();
        this.requestAnimation();
    }
    
    /**
     * Toggles the pause
     * @returns {Void}
     */
    showPause() {
        if (this.display.isPaused()) {
            this.endPause();
        } else {
            this.startPause();
        }
    }
    
    /**
     * Finish the Game
     * @returns {Void}
     */
    finishGame() {
        this.destroyGame();
        this.showMainScreen();
    }
    
    /**
     * Game Over
     * @returns {Void}
     */
    showGameOver() {
        this.display.set("gameOver").show();
        this.sound.end();
        this.scores.setInput();
        this.destroyGame();
    }
    
    /**
     * Show the High Scores
     * @returns {Void}
     */
    showHighScores() {
        this.display.set("highScores").show();
        this.scores.show();
    }
    
    /**
     * Saves the High Score
     * @returns {Void}
     */
    saveHighScore() {
        if (this.scores.save(this.score.level, this.score.score)) {
            this.showHighScores();
        }
    }
    
    /**
     * Show the Help
     * @returns {Void}
     */
    showHelp() {
        this.display.set("help").show();
    }

    /**
     * Destroys the game elements
     * @returns {Void}
     */
    destroyGame() {
        this.board.clearElements();
        this.tetriminos.clearElements();
    }
}




// Load the game
window.addEventListener("load", function () {
    "use strict";
    const game = new Game();
}, false);
