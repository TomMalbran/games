const Utils = (function () {
    "use strict";

    return {
        /**
         * Returns a random value between from and to
         * @param {Number} from
         * @param {Number} to
         * @returns {Number}
         */
        rand(from, to) {
            return Math.floor(Math.random() * (to - from + 1) + from);
        },
        
        /**
         * Returns the value higher than the min and lower than the max
         * @param {Number} value
         * @param {Number} min
         * @param {Number} max
         * @returns {Number}
         */
        clamp(value, min, max) {
            return Math.max(min, Math.min(max, value));
        },
    
        /**
         * Adds the separator every 3 decimals
         * @param {Number} number
         * @param {String} separator
         * @returns {String}
         */
        formatNumber(number, separator) {
            const value  = String(number);
            let   result = "";
            let   count  = 0;
            
            for (let i = value.length - 1; i >= 0; i -= 1) {
                const char = value.charAt(i);
                count += 1;
                result = char + result;
                
                if (count === 3 && i > 0) {
                    result = separator + result;
                    count  = 0;
                }
            }
            return result;
        },
        
        /**
         * Returns the angle between two values
         * @param {Number} x
         * @param {Number} y
         * @returns {Number}
         */
        calcAngle(x, y) {
            let angle = Math.round(Math.abs(Math.atan(y / x) * 180 / Math.PI));
            if (y < 0 && x >= 0) {
                angle = 360 - angle;
            } else if (y < 0 && x < 0) {
                angle = 180 + angle;
            } else if (y >= 0 && x < 0) {
                angle = 180 - angle;
            }
            return angle;
        },
        
        
        /**
         * Returns the closest element with an action
         * @param {Event} event
         * @returns {HTMLElement}
         */
        getTarget(event) {
            let element = event.target;
            while (element.parentElement && !element.dataset.action) {
                element = element.parentElement;
            }
            return element;
        },
        
        /**
         * Returns the position of an Element in the document
         * @param {HTMLElement} element
         * @returns {{top: Number, left: Number}}
         */
        getPosition(element) {
            let elem = element;
            let top  = 0;
            let left = 0;

            if (elem.offsetParent !== undefined) {
                top  = elem.offsetTop;
                left = elem.offsetLeft;
                
                while (elem.offsetParent && typeof elem.offsetParent === "object") {
                    elem = elem.offsetParent;
                    top  += elem.offsetTop;
                    left += elem.offsetLeft;
                }
            } else if (elem.x !== undefined) {
                top  = elem.y;
                left = elem.x;
            }
            return { top, left };
        },
        
        /**
         * Sets the position of the given element or elements
         * @param {HTMLElement} element
         * @param {Number} top
         * @param {Number} left
         * @returns {Void}
         */
        setPosition(element, top, left) {
            element.style.top  = top  + "px";
            element.style.left = left + "px";
        },
        
        /**
         * Removes the Element from the DOM
         * @param {HTMLElement} element
         * @returns {Void}
         */
        removeElement(element) {
            const parent = element.parentNode;
            parent.removeChild(element);
        },
    
    
        /**
         * Returns the Mouse Position
         * @param {Event} event
         * @returns {{top: Number, left: Number}}
         */
        getMousePos(event) {
            const e  = event || window.event;
            let top  = 0;
            let left = 0;

            if (e.pageX) {
                top  = e.pageY;
                left = e.pageX;
            } else if (e.clientX) {
                top  = e.clientY + (document.documentElement.scrollTop  || document.body.scrollTop);
                left = e.clientX + (document.documentElement.scrollLeft || document.body.scrollLeft);
            }
            return { top, left };
        },
    
        /**
         * Unselects the elements
         * @returns {Void}
         */
        unselect() {
            if (window.getSelection) {
                window.getSelection().removeAllRanges();
            } else if (document.selection) {
                document.selection.empty();
            }
        },
    };
}());
